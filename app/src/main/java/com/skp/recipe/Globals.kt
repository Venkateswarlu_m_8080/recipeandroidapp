package com.skp.recipe

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.skp.recipe.customviews.CustomProgressDialog
import com.skp.steve.Listeners.OnNegativeButtonClickListener
import com.skp.steve.Listeners.OnPositiveButtonClickListener
import java.io.*


object Globals
{


    fun log(currentObject: Any?, s: String)
    {
        Log.i("SKP", (currentObject?.javaClass?.simpleName ?: "null") + " :: " + s + " @ " + if (currentObject != null) currentObject.javaClass.canonicalName else "null")
    }


    fun toast(context: Context, message: String?)
    {
        Toast.makeText(context,message,Toast.LENGTH_LONG).show()
    }


    fun hideKeyboard(activity: Activity)
    {
        val view = activity.findViewById<View>(android.R.id.content)

        if (view != null)
        {
            val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }

    }



    fun showPositiveNegativeDialog(context: Context?, title: String?, message: String?, positive: String?, onPositiveButtonClickListener: OnPositiveButtonClickListener?, negative: String?, onNegativeButtonClickListener: OnNegativeButtonClickListener?)
    {
        val builder = AlertDialog.Builder(context)
        builder.setCancelable(false)
        builder.setTitle(title).setMessage(message).setPositiveButton(positive)
        { dialog: DialogInterface, which: Int ->
            if (onPositiveButtonClickListener != null) {
                onPositiveButtonClickListener.onPositiveButtonClicked(dialog)
            } else {
                dialog.dismiss()
            }
        }
        if (negative != null) builder.setNegativeButton(negative)
        { dialog: DialogInterface, which: Int ->
            if (onNegativeButtonClickListener != null) {
                onNegativeButtonClickListener.onNegativeButtonClicked(dialog)
            } else {
                dialog.dismiss()
            }
        }
        builder.show()
    }


    fun showAlert(context: Context?, title: String?, message: String?, positiveButtonText: String?)
    {
        showPositiveDialog(context, title, message, positiveButtonText, null)
    }

    fun showAlert(context: Context?, title: String?, message: String?)
    {
        showAlert(context, title, message, "OK")
    }

    fun showPositiveDialog(context: Context?, title: String?, message: String?, positive: String?, onPositiveButtonClickListener: OnPositiveButtonClickListener?)
    {
        showPositiveNegativeDialog(context, title, message, positive, onPositiveButtonClickListener, null, null)
    }


    fun getBytes(file: File?): ByteArray?
    {

        val size: Int = file!!.length().toInt()

        val byteArray = ByteArray(size)

        try
        {
            val buf = BufferedInputStream(FileInputStream(file))
            buf.read(byteArray, 0, byteArray.size)
            buf.close()
        }
        catch (e: FileNotFoundException)
        {
            e.printStackTrace()
        }
        catch (e: IOException)
        {
            e.printStackTrace()
        }


        return byteArray

    }



    fun showCustomProgressDialog(activity: Activity): CustomProgressDialog
    {

        val progressDialog = CustomProgressDialog(activity)
        progressDialog.show()
        progressDialog.setContentView(R.layout.progressdialog)
        progressDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        return progressDialog

    }


    fun dismissCustomProgressDialog(progressDialog: CustomProgressDialog?)
    {
        progressDialog?.dismiss()
    }


}






@BindingAdapter("imageResBytes")
fun setImageResId(imageView: AppCompatImageView, byteArray: ByteArray?)
{
    imageView.post{

        if (byteArray != null && byteArray.size > 0 && imageView.width > 0 && imageView.height > 0)
        {
            Glide.with(imageView.context).load(byteArray).apply(RequestOptions().override(imageView.width, imageView.height)).into(imageView);
        }

    }

}
