package com.skp.steve.Listeners

import android.content.DialogInterface



interface OnNegativeButtonClickListener
{
    fun onNegativeButtonClicked(dialog: DialogInterface?)
}