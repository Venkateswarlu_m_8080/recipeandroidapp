package com.skp.steve.Listeners

import android.content.DialogInterface


interface OnPositiveButtonClickListener {
    fun onPositiveButtonClicked(dialog: DialogInterface?)
}