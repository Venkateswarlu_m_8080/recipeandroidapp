package com.skp.recipe.Listeners

import com.skp.recipe.customviews.CustomProgressDialog

interface OnServiceResponseListener {
    fun onServiceResponse(
        responseString: String?,
        progressDialog: CustomProgressDialog?
    )
}