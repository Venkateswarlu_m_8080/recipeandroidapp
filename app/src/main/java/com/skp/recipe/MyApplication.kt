package com.skp.recipe

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate

class MyApplication : Application()
{
    override fun onCreate()
    {
        super.onCreate()
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
    }
}