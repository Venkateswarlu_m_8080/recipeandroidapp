package com.skp.recipe.activities

import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.crowdfire.cfalertdialog.CFAlertDialog
import com.crowdfire.cfalertdialog.CFAlertDialog.CFAlertActionAlignment
import com.crowdfire.cfalertdialog.CFAlertDialog.CFAlertActionStyle
import com.skp.recipe.Globals
import com.skp.recipe.R
import com.skp.recipe.aync.DeleteRecipe
import com.skp.recipe.aync.GetRecipe
import com.skp.recipe.aync.InsertRecipe
import com.skp.recipe.aync.UpdateRecipe
import com.skp.recipe.constants.Keys
import com.skp.recipe.customviews.CustomProgressDialog
import com.skp.recipe.database.RecipeData
import com.skp.recipe.databinding.ActivityAddRecipeBinding
import com.skp.recipe.pojos.RecipePojo
import com.skp.steve.Listeners.OnPositiveButtonClickListener
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import java.io.File


class AddRecipeActivity : AppCompatActivity()
{


    enum class Types
    {
        ADD, EDIT, VIEW
    }


    private lateinit var type: Types

    lateinit var binding: ActivityAddRecipeBinding


    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_recipe)

        binding.handler = this

        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        binding.recipePojo =   (intent.getSerializableExtra(RecipePojo::class.java.canonicalName) as RecipePojo?)?:run { RecipePojo()}


        type = (intent.getSerializableExtra(Types::class.java.canonicalName) as Types)

        supportActionBar?.title = binding.recipePojo!!.title?:run{ Keys.ADD_RECIPE}


        if (binding.recipePojo!!.id != null)
        {
            GetRecipe(this, binding.recipePojo!!.id!!,object: GetRecipe.OnResponseListener
            {
                override fun onResponse(recipedata: RecipeData?, progressDialog: CustomProgressDialog?)
                {

                    binding.recipePojo = recipedata!!.toRecipePojo()


                    updateUI()

                }

            }).execute()

        }





        updateUI()






    }


    private fun updateUI()
    {
        invalidateOptionsMenu()

        binding.invalidateAll()

    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean
    {

        menuInflater.inflate(R.menu.menu_add_recipe,menu)

        return true

    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean
    {

        menu?.findItem(R.id.action_save)?.isVisible = type.equals(Types.ADD) || type.equals(Types.EDIT)

        menu?.findItem(R.id.action_edit)?.isVisible = type.equals(Types.VIEW)

        menu?.findItem(R.id.action_delete)?.isVisible = type.equals(Types.VIEW)

        return true

    }





    override fun onOptionsItemSelected(item: MenuItem): Boolean
    {

        when (item.itemId)
        {
            android.R.id.home -> {finish()}
            R.id.action_edit -> {onEditClick()}
            R.id.action_save -> {onSaveClick()}
            R.id.action_delete -> {onDeleteClick()}
        }

        return true;

    }




    private fun onDeleteClick()
    {

        Globals.showPositiveNegativeDialog(this,"Confirm!","Do you want to delete this Recipe ?","Delete",object: OnPositiveButtonClickListener{
            override fun onPositiveButtonClicked(dialog: DialogInterface?) {

                DeleteRecipe(this@AddRecipeActivity,binding.recipePojo).execute()

            }
        },"Cancel",null)

    }


    fun onChooseCategoryClick()
    {


        if (type.equals(Types.VIEW))
        {
            return
        }


        val items = resources.getStringArray(R.array.categories)

        val builder = CFAlertDialog.Builder(this)
        builder.setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
        builder.setTitle("Select Category!")
        builder.setSingleChoiceItems(items, -1, object : DialogInterface.OnClickListener
        {
                override fun onClick(dialogInterface: DialogInterface?, index: Int)
                {


                    binding.recipePojo!!.category = items.get(index)

                    updateUI()



                }

        })
        builder.addButton(
            "DONE",
            resources.getColor(R.color.white),
            resources.getColor(R.color._FF4C4C),
            CFAlertActionStyle.POSITIVE,
            CFAlertActionAlignment.END,
            object : DialogInterface.OnClickListener {
                override fun onClick(dialogInterface: DialogInterface, i: Int) {


                    dialogInterface.dismiss()

                }
            })
        builder.show()

    }


    private fun onSaveClick()
    {

        if (binding.recipePojo!!.imageByteArray == null)
        {

            Globals.showAlert(this, "", "Please select Recipe image.")
            return
        }

        if (TextUtils.isEmpty(binding.recipePojo!!.title))
        {
            Globals.showAlert(this, "", "Please ${getString(R.string.enter_title_of_your_recipe)}.")
            return
        }

        if (TextUtils.isEmpty(binding.recipePojo!!.category) || binding.recipePojo!!.category.equals(getString(
                R.string.choose_category
            )))
        {
            Globals.showAlert(this, "", "Please ${getString(R.string.choose_category)}.")
            return
        }

        if (TextUtils.isEmpty(binding.recipePojo!!.ingredients))
        {
            Globals.showAlert(this, "", "Please Enter ${getString(R.string.ingredients)}.")
            return
        }

        if (TextUtils.isEmpty(binding.recipePojo!!.steps))
        {
            Globals.showAlert(this, "", "Please Enter ${getString(R.string.steps)}.")
            return
        }



        if (type.equals(Types.ADD))
        {
            InsertRecipe(this,binding.recipePojo).execute()
        }



        if (type.equals(Types.EDIT))
        {
            UpdateRecipe(this,binding.recipePojo).execute()
        }



    }




    private fun onEditClick()
    {


        type = Types.EDIT

        updateUI()


    }


    fun onAddPhotoClick()
    {

        if (type == Types.VIEW)
        {
            return
        }

        CropImage.activity().setGuidelines(CropImageView.Guidelines.ON).start(this);

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?)
    {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE)
        {
            val result = CropImage.getActivityResult(data)

            if (resultCode == Activity.RESULT_OK)
            {
                val resultUri = result.uri

                Globals.log(this, "resultUri : " + resultUri)

                Globals.log(this, "resultUri.path : " + resultUri.path)

                binding.recipePojo!!.imageByteArray = Globals.getBytes(File(resultUri.path))

                updateUI()

            }
            else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE)
            {
                val error = result.error
            }
        }
    }



    fun getEtFocusability(): Boolean
    {

        return type.equals(Types.ADD) || type.equals(Types.EDIT)

    }

    fun getImageLoadingVisibility(): Int
    {
        return if(type.equals(Types.VIEW) || type.equals(Types.EDIT) || binding.recipePojo!!.imageByteArray != null) {View.VISIBLE}else{View.GONE}
    }


    companion object
    {

        fun navigate(activity: Activity?, recipePojo: RecipePojo?, type: Types)
        {
            activity?.startActivity(Intent(activity, AddRecipeActivity::class.java).putExtra(RecipePojo::class.java.canonicalName,recipePojo).putExtra(Types::class.java.canonicalName, type))
        }

    }




}
