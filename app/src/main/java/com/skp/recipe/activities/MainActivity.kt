package com.skp.recipe.activities

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.PopupWindow
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView
import com.skp.recipe.R
import com.skp.recipe.adapters.FiltersRvAdapter
import com.skp.recipe.constants.Keys
import com.skp.recipe.databinding.FilterPopupLayoutBinding
import com.skp.recipe.pojos.FilterPojo
import kotlin.math.roundToInt


class MainActivity : AppCompatActivity()
{

    private lateinit var appBarConfiguration: AppBarConfiguration

    var filterPojosList = ArrayList<FilterPojo>()

    private lateinit var fab: FloatingActionButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        fab = findViewById(R.id.fab)

        fab.setOnClickListener { view -> AddRecipeActivity.navigate(this, null, AddRecipeActivity.Types.ADD) }

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)

        val navView: NavigationView = findViewById(R.id.nav_view)

        val navController = findNavController(R.id.nav_host_fragment)

        appBarConfiguration = AppBarConfiguration(setOf(R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow), drawerLayout)

        setupActionBarWithNavController(navController, appBarConfiguration)

        navView.setupWithNavController(navController)


        for (item in resources.getStringArray(R.array.categories))
        {
            filterPojosList.add(FilterPojo(item, true))
        }



    }


    fun hideFab()
    {
        fab.visibility = View.GONE
    }


    fun showFab()
    {
        fab.visibility = View.VISIBLE
    }



    override fun onCreateOptionsMenu(menu: Menu): Boolean
    {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean
    {

        when (item.itemId)
        {
            R.id.action_filter -> showCustomPopup(this, null)
        }

        return super.onOptionsItemSelected(item)

    }


    private fun showCustomPopup(context: Activity, view: View?)
    {

        val popupBinding: FilterPopupLayoutBinding = DataBindingUtil.inflate(layoutInflater, R.layout.filter_popup_layout, null, false)

        popupBinding.handler = this

        val popUpWindow = PopupWindow(context)

        popUpWindow.setContentView(popupBinding.root)

        popUpWindow.setWidth(LinearLayout.LayoutParams.WRAP_CONTENT)

        popUpWindow.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT)

        popUpWindow.setFocusable(true)

        popUpWindow.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        popupBinding.rvFilters.layoutManager = LinearLayoutManager(this)

        popupBinding.rvFilters.adapter = FiltersRvAdapter()

        (popupBinding.rvFilters.adapter as FiltersRvAdapter).updateItems(filterPojosList)

        (popupBinding.rvFilters.adapter as FiltersRvAdapter).setOnItemClickListener(object : FiltersRvAdapter.OnItemClickListener
        {
            override fun onItemClick(filterPojo: FilterPojo)
            {

                filterPojo.isChecked = !filterPojo.isChecked!!

                val selectedPojosList = (popupBinding.rvFilters.adapter as FiltersRvAdapter).getSelectedItems()

                LocalBroadcastManager.getInstance(this@MainActivity).sendBroadcast(Intent(Keys.ACTION_RELOAD_RECIPES).putExtra(Keys.FILTER_POJO_LIST, selectedPojosList))

                popUpWindow.dismiss()


            }
        })

        popUpWindow.showAtLocation(popupBinding.root, Gravity.TOP or Gravity.RIGHT, resources.getDimension(R.dimen._2sdp).roundToInt(), resources.getDimension(R.dimen._55sdp).roundToInt())

    }


    override fun onSupportNavigateUp(): Boolean
    {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

}
