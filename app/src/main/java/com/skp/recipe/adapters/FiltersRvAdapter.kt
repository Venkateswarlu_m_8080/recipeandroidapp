package com.skp.recipe.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.skp.recipe.R
import com.skp.recipe.databinding.FiltersRvItemBinding
import com.skp.recipe.pojos.FilterPojo

class FiltersRvAdapter : RecyclerView.Adapter<FiltersRvAdapter.MyViewHolder>()
{


    private lateinit var recyclerView: RecyclerView

    private var items: ArrayList<FilterPojo> = ArrayList()

    private var mainItems: ArrayList<FilterPojo> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder
    {
        return MyViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.filters_rv_item, parent, false), onItemClickListener)
    }



    override fun getItemCount(): Int
    {
        return items.size
    }


    override fun onBindViewHolder(holder: MyViewHolder, position: Int)
    {
        holder.bind(items.get(position))
    }


    fun updateItems(filterPojosList: List<FilterPojo>?) {

        mainItems.clear()
        items.clear()

        if (filterPojosList != null)
        {
            mainItems.addAll(filterPojosList)
            items.addAll(filterPojosList)
        }

        notifyDataSetChanged()

    }



    private var onItemClickListener: OnItemClickListener? = null

    fun setOnItemClickListener(onItemClickListener: OnItemClickListener) {
        this.onItemClickListener = onItemClickListener
    }


    interface OnItemClickListener {
        fun onItemClick(filterPojo: FilterPojo)
    }


    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    fun getSelectedItems(): java.util.ArrayList<FilterPojo>
    {

        val selectedItems = ArrayList<FilterPojo>()

        for (filterPojo in items)
        {
            if (filterPojo.isChecked!!)
            {
                selectedItems.add(filterPojo)
            }

        }


        return selectedItems

    }


    class MyViewHolder : RecyclerView.ViewHolder
    {


        var onItemClickListener: OnItemClickListener? = null

        val itemBinding: FiltersRvItemBinding
        constructor(itemBinding: FiltersRvItemBinding, onItemClickListener: OnItemClickListener?) : super(itemBinding.root)
        {
            this.itemBinding = itemBinding
            this.onItemClickListener = onItemClickListener
            this.itemBinding.handler = this;
        }

        fun bind(filterPojo: FilterPojo)
        {

            itemBinding.itemPojo = filterPojo

            itemBinding.executePendingBindings()

        }


        fun onItemClick(filterPojo: FilterPojo)
        {
            onItemClickListener?.onItemClick(filterPojo)
        }



    }


}

