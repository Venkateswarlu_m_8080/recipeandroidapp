package com.skp.recipe.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.skp.recipe.R
import com.skp.recipe.databinding.RecipeRvItemBinding
import com.skp.recipe.pojos.FilterPojo
import com.skp.recipe.pojos.RecipePojo

class RecipeRvAdapter : RecyclerView.Adapter<RecipeRvAdapter.MyViewHolder>()
{


    private lateinit var recyclerView: RecyclerView

    private var items: ArrayList<RecipePojo> = ArrayList()

    private var mainItems: ArrayList<RecipePojo> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder
    {
        return MyViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.recipe_rv_item, parent, false), onItemClickListener)
    }



    override fun getItemCount(): Int
    {
        return items.size
    }


    override fun onBindViewHolder(holder: MyViewHolder, position: Int)
    {
        holder.bind(items.get(position))
    }


    fun updateItems(recipePojosList: List<RecipePojo>?)
    {

        mainItems.clear()
        items.clear()

        if (recipePojosList != null)
        {
            mainItems.addAll(recipePojosList)
            items.addAll(recipePojosList)
        }

        notifyDataSetChanged()

    }



    private var onItemClickListener: OnItemClickListener? = null

    fun setOnItemClickListener(onItemClickListener: OnItemClickListener)
    {
        this.onItemClickListener = onItemClickListener
    }


    interface OnItemClickListener
    {
        fun onItemClick(recipePojo: RecipePojo)
    }


    override fun onAttachedToRecyclerView(recyclerView: RecyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }



    fun filterItemsByCategories(selectedCategories: List<FilterPojo>)
    {

        items = ArrayList()

        for (recipePojo in mainItems)
        {

            for (filterPojo in selectedCategories)
            {

                if (recipePojo.category.equals(filterPojo.category))
                {
                    items.add(recipePojo)
                }


            }

        }

        notifyDataSetChanged()

    }


    class MyViewHolder : RecyclerView.ViewHolder
    {


        var onItemClickListener: OnItemClickListener? = null

        val itemBinding: RecipeRvItemBinding

        constructor(itemBinding: RecipeRvItemBinding, onItemClickListener: OnItemClickListener?) : super(itemBinding.root)
        {
            this.itemBinding = itemBinding
            this.onItemClickListener = onItemClickListener
            this.itemBinding.handler = this;
        }

        fun bind(recipePojo: RecipePojo)
        {

            itemBinding.itemPojo = recipePojo

            itemBinding.executePendingBindings()

        }


        fun onItemClick(recipePojo: RecipePojo)
        {
            onItemClickListener?.onItemClick(recipePojo)
        }



    }


}

