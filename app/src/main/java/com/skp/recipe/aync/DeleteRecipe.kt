package com.skp.recipe.aync

import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.os.AsyncTask
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.skp.recipe.Globals
import com.skp.recipe.constants.Keys
import com.skp.recipe.customviews.CustomProgressDialog
import com.skp.recipe.database.DatabaseClient
import com.skp.recipe.pojos.RecipePojo
import com.skp.steve.Listeners.OnPositiveButtonClickListener

class DeleteRecipe(val activity: Activity, val recipePojo: RecipePojo?) : AsyncTask<Void?, Void?, Int>()
{

            var progressDialog: CustomProgressDialog? = null

            override fun onPreExecute()
            {
                super.onPreExecute()
                progressDialog = Globals.showCustomProgressDialog(activity)
            }



            override fun doInBackground(vararg params: Void?): Int
            {

                return DatabaseClient.getInstance(activity)!!.appDatabase.recipeDataDao()!!.delete(recipePojo?.toRecipeData())

            }


            override fun onPostExecute(deletedRecordsCount: Int)
            {
                super.onPostExecute(deletedRecordsCount)

                Globals.dismissCustomProgressDialog(progressDialog)


                if (deletedRecordsCount > 0)
                {
                    Globals.showPositiveDialog(activity,"Success!","Recipe Deleted Successfully.","OK",object: OnPositiveButtonClickListener { override fun onPositiveButtonClicked(dialog: DialogInterface?) {/*activity.setResult(RESULT_OK);*/LocalBroadcastManager.getInstance(activity).sendBroadcast(Intent(Keys.ACTION_RELOAD_RECIPES));activity.finish()}  })
                }
                else
                {
                    Globals.showAlert(activity,"Error!","Unable to Delete the Recipe! Please try again later.")
                }


            }


        }
