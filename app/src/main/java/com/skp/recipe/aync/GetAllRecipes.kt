package com.skp.recipe.aync

import android.app.Activity
import android.os.AsyncTask
import com.skp.recipe.Globals
import com.skp.recipe.customviews.CustomProgressDialog
import com.skp.recipe.database.DatabaseClient
import com.skp.recipe.database.RecipeData
import com.skp.recipe.pojos.RecipePojo

class GetAllRecipes(val activity: Activity, val onResponseListener: OnResponseListener) : AsyncTask<Void?, Void?, List<RecipeData?>>()
{

    var progressDialog: CustomProgressDialog? = null

    override fun onPreExecute()
    {
        super.onPreExecute()
        progressDialog = Globals.showCustomProgressDialog(activity)
    }


    override fun doInBackground(vararg params: Void?): List<RecipeData?>?
    {
        return DatabaseClient.getInstance(activity)!!.appDatabase.recipeDataDao()!!.getAll
    }


    override fun onPostExecute(allRecipes: List<RecipeData?>)
    {
        super.onPostExecute(allRecipes)

        Globals.dismissCustomProgressDialog(progressDialog)

        val recipePojoList = ArrayList<RecipePojo>()


        for (recipeData in allRecipes)
        {
            recipePojoList.add(recipeData!!.toRecipePojo()!!)
        }


        onResponseListener.onResponse(recipePojoList, progressDialog)


    }


    interface OnResponseListener
    {
        fun onResponse(recipePojoList: List<RecipePojo>?, progressDialog: CustomProgressDialog?)
    }


}
