package com.skp.recipe.aync

import android.app.Activity
import android.os.AsyncTask
import com.skp.recipe.Globals
import com.skp.recipe.customviews.CustomProgressDialog
import com.skp.recipe.database.DatabaseClient
import com.skp.recipe.database.RecipeData

class GetRecipe(val activity: Activity, val recipeId: Int, val onResponseListener: OnResponseListener) : AsyncTask<Void?, Void?, RecipeData>() {

    var progressDialog: CustomProgressDialog? = null

    override fun onPreExecute()
    {
        super.onPreExecute()
        progressDialog = Globals.showCustomProgressDialog(activity)
    }


    override fun doInBackground(vararg params: Void?): RecipeData?
    {

        return DatabaseClient.getInstance(activity)!!.appDatabase.recipeDataDao()!!.getRecipeById(recipeId)

    }


    override fun onPostExecute(recipedata: RecipeData?)
    {

        super.onPostExecute(recipedata)

        Globals.dismissCustomProgressDialog(progressDialog)

        onResponseListener.onResponse(recipedata, progressDialog)


    }


    interface OnResponseListener
    {
        fun onResponse(recipedata: RecipeData?, progressDialog: CustomProgressDialog?)
    }


}
