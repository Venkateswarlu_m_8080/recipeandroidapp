package com.skp.recipe.aync

import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.os.AsyncTask
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.skp.recipe.Globals
import com.skp.recipe.constants.Keys
import com.skp.recipe.customviews.CustomProgressDialog
import com.skp.recipe.database.DatabaseClient
import com.skp.recipe.pojos.RecipePojo
import com.skp.steve.Listeners.OnPositiveButtonClickListener

class UpdateRecipe(val activity: Activity, val recipePojo: RecipePojo?) : AsyncTask<Void?, Void?, Int>()
{

            var progressDialog: CustomProgressDialog? = null

            override fun onPreExecute()
            {
                super.onPreExecute()
                progressDialog = Globals.showCustomProgressDialog(activity)
            }



            override fun doInBackground(vararg params: Void?): Int
            {

                return DatabaseClient.getInstance(activity)!!.appDatabase.recipeDataDao()!!.update(recipePojo?.toRecipeData())

            }


            override fun onPostExecute(updatedRecordsCount: Int)
            {
                super.onPostExecute(updatedRecordsCount)

                Globals.dismissCustomProgressDialog(progressDialog)

                if (updatedRecordsCount > 0)
                {
                    Globals.showPositiveDialog(activity,"Success!","Recipe Updated Successfully.","OK",object: OnPositiveButtonClickListener { override fun onPositiveButtonClicked(dialog: DialogInterface?) {/*activity.setResult(RESULT_OK);*/LocalBroadcastManager.getInstance(activity).sendBroadcast(Intent(Keys.ACTION_RELOAD_RECIPES));activity.finish()}  })
                }
                else
                {
                    Globals.showAlert(activity,"Error!","Unable to Update the Recipe! Please try again later.")
                }


            }


        }
