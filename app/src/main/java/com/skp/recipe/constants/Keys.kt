package com.skp.recipe.constants


object Keys
{

    val ACTION_RELOAD_RECIPES = "ACTION_RELOAD_RECIPES"
    val ADD_RECIPE = "Add Recipe"
    val CHOOSE_CATEGORY = "Choose Category"
    val FILTER_POJO_LIST = "FILTER_POJO_LIST"

}