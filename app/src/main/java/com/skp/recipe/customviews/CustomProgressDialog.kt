package com.skp.recipe.customviews

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.view.Window

class CustomProgressDialog : Dialog
{
    constructor(context: Context) : super(context){initialize()}
    constructor(context: Context, themeResId: Int) : super(context, themeResId){initialize()}
    constructor(context: Context, cancelable: Boolean, cancelListener: DialogInterface.OnCancelListener?) : super(context, cancelable, cancelListener){initialize()}
    private fun initialize()
    {
        window!!.requestFeature(Window.FEATURE_NO_TITLE)
        setCancelable(false)
    }

}