package com.skp.recipe.database

import androidx.room.Database
import androidx.room.RoomDatabase


@Database(entities = [RecipeData::class], version = 1)
abstract class AppDatabase : RoomDatabase()
{
    abstract fun recipeDataDao(): RecipeDataDao?
}