package com.skp.recipe.database

import android.content.Context
import androidx.room.Room
import com.skp.recipe.R

class DatabaseClient private constructor(private val mCtx: Context) {

    val appDatabase: AppDatabase

    companion object
    {
        private var mInstance: DatabaseClient? = null

        @Synchronized
        fun getInstance(mCtx: Context?): DatabaseClient?
        {
            if (mInstance == null)
            {
                mInstance = DatabaseClient(mCtx!!)
            }
            return mInstance
        }
    }

    init
    {
        appDatabase = Room.databaseBuilder(mCtx, AppDatabase::class.java, mCtx.resources.getString(R.string.app_name)).build()
    }


}