package com.skp.recipe.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.skp.recipe.pojos.RecipePojo
import java.io.Serializable

@Entity(tableName = "recipe_table")
class RecipeData : Serializable
{

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "recipe_id")
    var id: Int? = null

    @ColumnInfo(name = "image", typeAffinity = ColumnInfo.BLOB)
    var image: ByteArray? = null

    @ColumnInfo(name = "title")
    var title: String? = null


    @ColumnInfo(name = "category")
    var category: String? = null


    @ColumnInfo(name = "ingredients")
    var ingredients: String? = null


    @ColumnInfo(name = "steps")
    var steps: String? = null


    fun toRecipePojo(): RecipePojo?
    {
        val recipePojo = RecipePojo()
        recipePojo.id = id
        recipePojo.imageByteArray = image
        recipePojo.title = title
        recipePojo.category = category
        recipePojo.ingredients = ingredients
        recipePojo.steps = steps
        return recipePojo

    }




}