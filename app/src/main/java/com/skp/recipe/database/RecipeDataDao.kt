package com.skp.recipe.database

import androidx.room.*

@Dao
interface RecipeDataDao
{
    @get:Query("SELECT * FROM recipe_table")
    val getAll: List<RecipeData?>?

    @Query("SELECT * FROM recipe_table WHERE recipe_id = :id")
    fun getRecipeById(id: Int?): RecipeData?

    @Insert
    fun insert(recipeData: RecipeData?): Long


    @Delete
    fun delete(recipeData: RecipeData?): Int

    @Update
    fun update(recipeData: RecipeData?): Int


}