package com.skp.recipe.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.skp.recipe.Globals
import com.skp.recipe.R
import com.skp.recipe.activities.MainActivity
import com.skp.recipe.databinding.FragmentAboutUsBinding


class AboutUsFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        val binding: FragmentAboutUsBinding = DataBindingUtil.inflate(inflater,R.layout.fragment_about_us, container, false)

        binding.wvAboutUs.getSettings().setJavaScriptEnabled(true);

        binding.wvAboutUs.getSettings().setLoadWithOverviewMode(true)

        binding.wvAboutUs.getSettings().setUseWideViewPort(true)

        val progDailog = Globals.showCustomProgressDialog(requireActivity())

        binding.wvAboutUs.setWebViewClient(object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                progDailog.show()
                view.loadUrl(url)
                return true
            }

            override fun onPageFinished(view: WebView, url: String) {
                progDailog.dismiss()
            }
        })

        binding.wvAboutUs.loadUrl("https://www.fourtitude.asia/about/who-we-are/about-fourtitude-asia#")

        setHasOptionsMenu(true)

        (requireActivity() as MainActivity).hideFab()

        return binding.root

    }


    override fun onPrepareOptionsMenu(menu: Menu) {
        menu.clear()
    }
}
