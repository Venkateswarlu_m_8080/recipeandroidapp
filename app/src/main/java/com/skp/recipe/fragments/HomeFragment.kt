package com.skp.recipe.fragments

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.GridLayoutManager
import com.skp.recipe.R
import com.skp.recipe.activities.AddRecipeActivity
import com.skp.recipe.activities.MainActivity
import com.skp.recipe.adapters.RecipeRvAdapter
import com.skp.recipe.aync.GetAllRecipes
import com.skp.recipe.constants.Keys
import com.skp.recipe.customviews.CustomProgressDialog
import com.skp.recipe.databinding.FragmentHomeBinding
import com.skp.recipe.pojos.FilterPojo
import com.skp.recipe.pojos.RecipePojo


class HomeFragment : Fragment()
{

    lateinit var binding: FragmentHomeBinding

    val reloadBroadCastReceiver  = object: BroadcastReceiver()
    {
        override fun onReceive(context: Context?, intent: Intent?)
        {


            var filterList = intent?.getSerializableExtra(Keys.FILTER_POJO_LIST)


            if (filterList != null)
            {
                getAllRecipesFromDB(filterList as ArrayList<FilterPojo>)
            }
            else
            {
                getAllRecipesFromDB(null)
            }





        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {

        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_home, container, false)

        binding.rvRecipes.layoutManager = GridLayoutManager(context,2)

        binding.rvRecipes.adapter = RecipeRvAdapter()



        getAllRecipesFromDB(null)


        (binding.rvRecipes.adapter as RecipeRvAdapter).setOnItemClickListener(object: RecipeRvAdapter.OnItemClickListener
        {

            override fun onItemClick(recipePojo: RecipePojo)
            {
                recipePojo.imageByteArray = null

                AddRecipeActivity.navigate(activity, recipePojo, AddRecipeActivity.Types.VIEW)
            }
        })


        LocalBroadcastManager.getInstance(requireContext()).registerReceiver(reloadBroadCastReceiver, IntentFilter(Keys.ACTION_RELOAD_RECIPES))


        (requireActivity() as MainActivity).showFab()


        return binding.root


    }


    override fun onDetach() {
        super.onDetach()
        LocalBroadcastManager.getInstance(requireContext()).unregisterReceiver(reloadBroadCastReceiver)
    }




    private fun getAllRecipesFromDB(filterPojoList: List<FilterPojo>?)
    {

        GetAllRecipes(requireActivity(), object: GetAllRecipes.OnResponseListener
        {
            override fun onResponse(recipePojoList: List<RecipePojo>?, progressDialog: CustomProgressDialog?)
            {

                (binding.rvRecipes.adapter as RecipeRvAdapter).updateItems(recipePojoList)


                if (filterPojoList != null)
                {
                    (binding.rvRecipes.adapter as RecipeRvAdapter).filterItemsByCategories(filterPojoList)
                }


                binding.rvRecipes.visibility = if((binding.rvRecipes.adapter as RecipeRvAdapter).itemCount > 0) View.VISIBLE else View.GONE



            }

        }).execute()

    }



}
