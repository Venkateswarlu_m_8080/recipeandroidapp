package com.skp.recipe.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.skp.recipe.R

class SampleFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {

        requireActivity().finish()

        setHasOptionsMenu(true)

        return inflater.inflate(R.layout.fragment_exit_app, container, false)
    }


    override fun onPrepareOptionsMenu(menu: Menu) {
        menu.clear()
    }

}
