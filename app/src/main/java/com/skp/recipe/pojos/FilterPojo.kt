package com.skp.recipe.pojos

import java.io.Serializable

class FilterPojo(var category: String?, var isChecked: Boolean?) : Serializable
{
    override fun toString(): String = "FilterPojo(category=$category, isChecked=$isChecked)"
}