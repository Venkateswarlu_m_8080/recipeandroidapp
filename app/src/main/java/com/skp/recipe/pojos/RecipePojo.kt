package com.skp.recipe.pojos

import com.skp.recipe.constants.Keys.CHOOSE_CATEGORY
import com.skp.recipe.database.RecipeData
import java.io.Serializable

class RecipePojo(): Serializable
{


    var id: Int? = null
    var imageByteArray: ByteArray? = null
    var title: String? = null
    var category: String? = CHOOSE_CATEGORY
    var ingredients: String? = ""
    var steps: String? = ""

    override fun toString(): String
    {
        return "RecipePojo(id=$id, imageResId=$imageByteArray, title='$title', category='$category', ingredients='$ingredients', steps='$steps')"
    }



    fun toRecipeData(): RecipeData?
    {

        val recipeData = RecipeData()

        recipeData.id = id
        recipeData.image = imageByteArray
        recipeData.title = title
        recipeData.category = category
        recipeData.ingredients = ingredients
        recipeData.steps = steps

        return recipeData

    }



}